angular.module('app').controller('CadastroController', CadastroController);
CadastroController.$inject = ['$location', 'CursoService', '$routeParams'];

    function CadastroController($location, CursoService, $routeParams){
        vm = this;
        vm.teste = 'Cadastro';
        vm.cliente = {};
        vm.idCli = undefined;
        vm.textoBotao = 'Cadastrar';
        vm.tipoBotao = "btn btn-primary";
        vm.show = false;

        if($routeParams.idCli){
            vm.idCli = $routeParams.idCli;
            buscarID(vm.idCli);
            vm.textoBotao = 'Editar';
            vm.tipoBotao = 'btn btn-warning';
            vm.show = true;
        }
      
        vm.navegar = function(){
            $location.path('/')
        }

        vm.cadastrar = function(){
            if(vm.textoBotao == 'Cadastrar'){
                CursoService.exec_POST(vm.cliente)
                        .then(function(resposta){
                            if(resposta){
                                vm.clientes = resposta;
                            }
                        })
            }else if(vm.textoBotao == 'Editar'){
                CursoService.exec_PUT(vm.cliente)
                     .then(function(resposta){
                         if(resposta){
                            vm.clientes = resposta;
                         }
                })
            }
            
            vm.navegar('/');
        }

        function buscarID(id){
            CursoService.exec_GET_ID(id)
                        .then(function(resposta){
                            if(resposta){
                                vm.cliente  = resposta;
                            }
                        })
        }

        vm.limpar = function(){
            vm.cliente = {};
        }

        vm.buscarCEP = function(cep){
            CursoService.exec_GET_CEP(cep).then(function(resposta){
                if(resposta){
                    vm.cliente.endereco = resposta;
                    vm.cliente.endereco.numero = '';
                    console.log(vm.cliente.endereco)
                }else{
                    vm.endereco = null;
                }
            })
        }

    }