angular.module('app').controller('HomeController', HomeController);
HomeController.$inject = ['$location', 'CursoService'];

    function HomeController($location, CursoService){
        vm = this;
        vm.teste = 'Home';
        vm.clientes = '';
        vm.uf = '';
        vm.errorBd = false;
        
        vm.init = function(){
            vm.listarClientes();
        }

        vm.navegar = function(rota, id){
            $location.path(rota + '/' + id);
        }

        vm.listarClientes = function(){
            CursoService.exec_GET().then(function(resposta){
                if(resposta){
                    vm.clientes = resposta;
                    vm.errorBd = false;
                }else{
                    vm.errorBd = true;
                    vm.clientes = null;
                }
            })
        }


        vm.deletar = function(id){
            CursoService.exec_DEL(id).then(function(resposta){
                if(resposta){
                    //Mensagem de Resposta
                }
            })
        }

        vm.editar = function(id){
            vm.navegar('Cadastro', id)
        }
        
    }