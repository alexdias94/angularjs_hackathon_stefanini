angular.module('app').component('alertaParaUsuario', {
    templateUrl: 'components/alert.html',
    bindings: {
        msg: '=',
        tipo: '='
    },
    controllerAs: 'vm',
    controller: function(){
        vm = this;
        vm.tipoClass = undefined;

        vm.$onInit = function(){
            console.log(vm.tipo)
            if(vm.tipo == 'sucesso'){
                vm.tipoClass = 'alert alert-success';
            }else if(vm.tipo == 'danger'){
                vm.tipoClass = 'alert alert-danger';
            }else if(vm.tipo == 'warning'){
                vm.tipoClass = 'alert alert-warning';
            }
        }
    }
})